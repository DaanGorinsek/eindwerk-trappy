/**
 * Created by daang_000 on 8/06/2016.
 */

/* custom scrollbar voor klantenlijst */


$(function() {
    var window_height = $(window).height(),
        content_height = window_height - 300;

    $('.wrapper').height(content_height);
});

$( window ).resize(function() {
    var window_height = $(window).height(),
        content_height = window_height - 300;
    $('.wrapper').height(content_height);
})